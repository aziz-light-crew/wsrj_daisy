### wsrj_daisy ###
Power and data distribution board for Octows2811 Adaptor style LED controllers. 
This board takes the first output and combines it with supplied power and shifts the 2nd output to the first output of the output jack. 
